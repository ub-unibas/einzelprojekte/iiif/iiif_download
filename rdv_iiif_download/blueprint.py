from flask import send_file
from flask_restx import Resource, Namespace
import importlib
from pathvalidate import sanitize_filename
from pkg_resources import Requirement, resource_filename


ns_iiif_dl = Namespace('dl', description="Donwload Service for content of IIIF manifest")

dl_iiif = importlib.import_module("rdv_iiif_download.downloadservice-zeitungsartikel.download_zas")

@ns_iiif_dl.route('/iiif/<path:manifest_path>', methods=['GET', 'POST'])
class IIIFDownload(Resource):

    def get(self, manifest_path):
        from urllib.parse import unquote, quote_plus
        #quoted_manif_path = quote_plus(manifest_path, safe="")
        #print(quoted_manif_path)
        manifest_path = unquote(manifest_path)
        print("TEST",manifest_path)
        zip_file = dl_iiif.download_iiif_resource(manifest_path, max=5, style_dir_path=resource_filename(Requirement.parse("rdv_iiif_download"), "rdv_iiif_download/static/style"))
        zip_file.seek(0)
        filename= sanitize_filename("_".join([k for k in manifest_path.split("/")[3:] if k]))
        return send_file(zip_file, mimetype='application/zip', attachment_filename="{}.zip".format(filename), as_attachment=True)