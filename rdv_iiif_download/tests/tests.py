from flask import Flask, Blueprint
from flask_restx import Api

from rdv_iiif_download import ns_iiif_dl
debug = True
api_v1 = Blueprint('iiif_download', __name__, url_prefix='')
rdv_api_v1 = Api(api_v1, doc='/doc/', version="0.0.2", title='IIIF Download API',
                    description='APIs for Downloading IIIF')
rdv_api_v1.add_namespace(ns_iiif_dl)

rdv_query_app = Flask(__name__)
rdv_query_app.register_blueprint(api_v1)
rdv_query_app.run(debug=debug, port=5055, ssl_context='adhoc')
# https://127.0.0.1:5055/dl/iiif/https%3A%2F%2F127.0.0.1%3A5001%2Fzas_flex_manifest%2F4eb1cb0e1d45a984e9f269855699bf1a%2Fflex_manifest%2F