import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rdv_iiif_download", # Replace with your own username
    version="0.0.2",
    author="Vera Bieri, Martin Reisacher",
    author_email="vera.bieri@unibas.ch",
    description="flask blueprinf for iiif download",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    install_requires=['flask<1.2', 'flask-restx', 'pathvalidate'],
    python_requires=">=3.6",
    include_package_data=True,
    zip_safe=False
)